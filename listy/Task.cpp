
#include <iostream>
#include <sstream> 
#include <fstream> 
#include <iostream>
#include <iomanip>
#include "Funct.h"
#include "Task.h"
#include "Project.h"
#include "LesData3.h"

using namespace std;

/**
 *	Reads info about the task from the user.
 */
void Task::readD() {
	priority = lesInt("\tTask priority", 1, 9);
	cout << "\tTask name: "; getline(cin, name);
	cout << "\tTask description: "; getline(cin, desc);
	cout << "\tTask category: "; getline(cin, category);
	start = readDate("Task starting date");
	deadline = readDate("Task deadline");
	// Read times
}

/**
 *	Writes info about this task on the screen.
 */
void Task::writeD() {
	cout
		<< "\n|" << setw(3) << id
		<< " |" << setw(12) << name
		<< " |" << setw(9) << category
		<< " |" << setw(5) << priority
		<< " |" << setw(12) << writeDate(start)
		<< " |" << setw(12) << writeDate(deadline)
		<< " |" << setw(12) << writeDate(ended) << " |";
}

/**
 *	Writes a line of info about this task on the screen.
 *
 *	Selects color based on the status of the task
 *
 *  @see  writeDate(...)
 *  @see  getStatusColor(...)
 *  @see  printInColor(...)
 */
void Task::writeLine() {
	stringstream ss;
	ss
		<< "\n|" << setw(3) << order
		<< " |" << setw(12) << name
		<< " |" << setw(9) << category
		<< " |" << setw(5) << priority
		<< " |" << setw(12) << writeDate(start)
		<< " |" << setw(12) << writeDate(deadline)
		<< " |" << setw(12) << writeDate(ended) << " |";
	ConsoleColor color = getStatusColor(status);
	printInColor(ss.str(), color);
}


/**
 *  Writes the task to a file
 *
 *  @param		out  - The file to write to
 *  @see			writeDate(...)
 */
void Task::writeToFile(ofstream& out) {
	out << id << " " << priority << " " << order << " " << (int)status << " " 
		<< writeDate(start) << " " 
		<< writeDate(ended) << " "
		<< writeDate(deadline) << " "
		<< category << "\n"
		<< name << "\n"
		<< desc << "\n";
}

/**
 *  Reads the task from a file
 *
 *  @param		out  - The file to write to
 *  @see			parseDate(...)
 */
void Task::readFromFile(ifstream& inn) {
	inn >> priority; inn.ignore();
	inn >> order; inn.ignore();

	// Read the int of the status and cast to Status
	int statusInt;
	inn >> statusInt; inn.ignore();
	status = (Status)statusInt;
	
	string str;
	inn >> str; start = parseDate(str); inn.ignore();
	inn >> str; ended = parseDate(str); inn.ignore();
	inn >> str; deadline = parseDate(str); inn.ignore();

	getline(inn, category);
	getline(inn, name);
	getline(inn, desc);
}

