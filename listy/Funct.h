#pragma once

#include "Task.h"
#include "User.h"
#include "Project.h"
#include "Struct.h"



void divLine(int amount);
bool compareDates(const Date& a, const Date& b);

std::string writeDate(Date date);
Date parseDate(std::string in);

Date readDate(std::string text = "Input date");
Status readStatus();
ConsoleColor getStatusColor(Status status);
void printInColor(std::string str, ConsoleColor color);
void awaitEnter();

void startUp();
void shutDown();

void loadUsers();
void loadProjects();

void loadGlobals();
void saveGlobals();

int getNextId();


void mainMenu();
void printMainMenu();

void printProjects();
void addProject();
void removeProject();
void editProject();
void printProjectDesc();

void projectMenu(Project* const proj);
void printProjectMenu(Project* const proj);

void editProjectDetail(Project* const proj);
void printProjectDetails(Project* const proj);

void printTasks(Project* const proj);
void addTask(Project* const proj);
void removeTask(Project* const proj);

void editTask(Project* const proj);
void printTaskDetails(Task* const proj);

void printMembers(Project* const proj);
void addMember(Project* const proj);
void removeMember(Project* const proj);


void userMenu();
void printUserMenu();

void printUsers();
void addUser();
void removeUser();
void editUser();
void printUserDetails(User* const proj);


void helpMenu();
void printHelpMenu();
void helpUser();
void helpProject();
void helpTask();


