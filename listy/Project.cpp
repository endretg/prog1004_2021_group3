
#include <iostream>
#include <fstream> 
#include <filesystem>
#include <sstream> 
#include <string>
#include <iomanip>

#include "Funct.h"
#include "Project.h"
#include "LesData3.h"
#include "Const.h"

using namespace std;
using namespace std::filesystem;


extern std::map<int, Project*> gProjects;
extern std::map<int, User*> gUsers;

/**
 *	Reads info about the project from the user.
 */
void Project::readD() {
	priority = lesInt("\tProject priority", 1, 9);
	cout << "\tProject name: "; getline(cin, name);
	cout << "\tProject description: "; getline(cin, desc);
	cout << "\tProject category: "; getline(cin, category);
	start = readDate("Project starting date");
	deadline = readDate("Project deadline");
	// Read times
}

/**
 *	Writes info about this project on the screen. 
 */
void Project::writeD() {
	std::cout << "\n\t" << id << ": " << name << " - " << writeDate(deadline);
}


/**
 *	Writes a line of info about this project on the screen. 
 *
 *	Selects color based on the status of the project
 *
 *  @see  writeDate(...)
 *  @see  getStatusColor(...)
 *  @see  printInColor(...)
 */
void Project::writeLine() {
	stringstream ss;
	ss
		<< "\n|" << setw(3) << id
		<< " |" << setw(12) << name
		<< " |" << setw(9)  << category
		<< " |" << setw(5)  << priority
		<< " |" << setw(12) << writeDate(start)	
		<< " |" << setw(12) << writeDate(deadline)
		<< " |" << setw(12) << writeDate(ended) << " |";	

	ConsoleColor color = getStatusColor(status);
	printInColor(ss.str(), color);
	//Add status here later!
}

/**
 *	Writes out the name and descriptopm of this project onto the screen.
 */
void Project::writeDesc() {
	std::cout
		<< left
		<< "\n| " << setw(12) << name
		<< " | " << setw(61) << desc
		<< " |";
}


/**
 *	Deletes the files belonging to this project
 *
 *  @see  Project::getPath()
 */
void Project::deleteObject() {
	string p = getPath();
	remove_all(p);
}


/**
 *	Writes info about this project to files
 *
 *  @see  Project::writeDetails(...)
 *  @see  Project::writeTasks(...)
 *  @see  Project::getPath()
 */
void Project::writeToFile() {
	string p = getPath();

	if(!is_directory(p)) {
		create_directories(p);
	}

	ofstream out;
	out.open(p + "info.dta", ofstream::trunc);

	if(out) {
		writeDetails(out);
	}

	out.close();
	out.open(p + "tasks.dta", ofstream::trunc);

	if(out) {
		writeTasks(out);
	}

	out.close();

}

/**
 *	Reads info about this project from files
 * 
 *  @see  Project::readDetails(...)
 *  @see  Project::readTasks(...)
 *  @see  Project::getPath()
 */
void Project::readFromFile() {
	string p = getPath();

	ifstream inn;
	inn.open(p + "info.dta");

	if(inn) {
		readDetails(inn);
	}

	inn.close();
	inn.open(p + "tasks.dta");

	if(inn) {
		readTasks(inn);
	}

	inn.close();
}

/**
 *	Gets the path for files about this project
 *
 *	@return		The path to the folder for this project
 */
string Project::getPath() {
	ostringstream oss;
	oss << PROJECTS_ROOT << id << "/";
	return oss.str();
}

/**
 *	Sets the order of each task in the project to their index
 */
void Project::updateTaskOrder() {
	for(int i = 0; i < tasks.size(); ++i) {
		tasks[i]->order = i+1;
	}
}


/**
 *  Writes the tesks of the project to a file
 *
 *  @param   out  - The file to write to
 *  @see  Task::writeToFile(...)
 */
void Project::writeTasks(ofstream& out) {

	for(auto task : tasks) {
		task->writeToFile(out);
	}

}

/**
 *  Reads the tasks of the project from a file
 *
 *  @param   inn  - The file to read from
 *  @see  Task::readFromFile(...)
 */
void Project::readTasks(ifstream& inn) {
	// Read in and add the tasks
	int id;
	while(inn >> id) {
		auto task = new Task(id);
		task->readFromFile(inn);
		tasks.emplace_back(task);
	}
}

/**
 *  Writes the details of the project to a file
 *
 *  @param   out  - The file to write to
 */
void Project::writeDetails(ofstream& out) {
	out << priority << " " << (int) status << " " 
		<< writeDate(start) << " " 
		<< writeDate(ended) << " "
		<< writeDate(deadline) << " "
		<< category << "\n"
		<< name << "\n"
		<< desc << "\n";

	for(auto pair : members)
		out << pair.first << '\n';
	
}

/**
 *  Reads the details of the project from a file
 *
 *  @param   inn  - The file to read from
 */
void Project::readDetails(ifstream& inn) {
	inn >> priority; inn.ignore();
	
	// Read the int of the status and cast to Status
	int statusInt;
	inn >> statusInt; inn.ignore(); 
	status = (Status) statusInt;
	
	string str;
	inn >> str; start = parseDate(str); inn.ignore();
	inn >> str; ended = parseDate(str); inn.ignore();
	inn >> str; deadline = parseDate(str); inn.ignore();

	getline(inn, category);
	getline(inn, name);
	getline(inn, desc);

	// Read in the member ids
	int id;
	while(inn >> id) {
		inn.ignore();

		// Try to get and add the user with that id, move on if they're not found (User was deleted)
		try {
			auto user = gUsers.at(id);
			members[id] = user;
		} catch(const out_of_range& ex) {}

	}

}
