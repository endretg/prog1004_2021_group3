
#include "Funct.h"
#include "Const.h"
#include "LesData3.h"
#include <iostream>
#include <filesystem>
#include <fstream> 
#include <string> 
#include <sstream> 
#include <windows.h>
#include <algorithm>               //  algorithm

using namespace std;
using namespace std::filesystem;


extern int gNextId;

extern std::map<int, Project*> gProjects;
extern std::map<int, User*> gUsers;



/**
 *  Prints a divider of a certail width onto the screen.
 */
void divLine(int amount) {
	cout << "\n+";
	for(int i = 0; i < amount; i++) {
		cout << "-";
	}
	cout << "+";
}


/**
 *  Compares 2 dates
 *
 *  @param		a  - The first date
 *  @param		a  - The second date
 *	@return		Wherher the second date is bigger than the first
 */
bool compareDates(const Date& a, const Date& b) {
	int comp = b.year - a.year;
	if(comp > 0) return true;
	if(comp < 0) return false;

	comp = b.month - a.month;
	if(comp > 0) return true;
	if(comp < 0) return false;

	comp = b.day - a.day;
	if(comp > 0) return true;
	
	return false;
}

/**
 *  Converts the date to a string for console and files
 *
 *  @param		date  - The date to convert
 *	@return		A string representing the date in the format yyyy/mm/dd
 */
string writeDate(Date date) {
	stringstream ss;
	ss << setw(4) << setfill('0') << date.year << "/"
		 << setw(2) << setfill('0') << date.month << "/"
		 << setw(2) << setfill('0') << date.day;
	return ss.str();
}

/**
 *  Converts a string into a date
 *
 *  @param		in  - A string in the format yyyy/mm/dd
 *	@return		The date that string represents
 */
Date parseDate(string in) {
	Date res;
	sscanf_s(in.c_str(), "%4d/%2d/%2d",
		&res.year,
		&res.month,
		&res.day);
	return res;
}

/**
 *  Requests a date from the user
 *
 *  @param		text - The text to show the user
 *	@return		The date the user passed in.
 *	@see			parseDate(...)
 */
Date readDate(string text) {
	cout << "\n\t" << text << " (dd/mm/yyyy): ";
	string out; getline(cin, out);
	return parseDate(out);
}


/**
 *  Requests a status from the user
 *
 *	@return		The status the user passed in.
 */
Status readStatus() {
	char type;

	while(true) {
		type = lesChar("\tInput a status [ T (ToDo), S (Started), C (Completed), F (Failed) ]");

		switch(type) {
		case 'T': return Status::ToDo;
		case 'S': return Status::Started;
		case 'C': return Status::Completed;
		case 'F': return Status::Failed;
		}
	}
}

/**
 *  Returns the color for a status
 *
 *  @param		status - The status we want the color for
 *	@return		The appropriate color for that status
 *	@see			parseDate(...)
 */
ConsoleColor getStatusColor(Status status) {
	switch(status) {
	case Status::Completed:		return ConsoleColor::Green;
	case Status::Started:			return ConsoleColor::Blue;
	case Status::Failed:			return ConsoleColor::Red;
	default:									return ConsoleColor::White;
	}

}

/**
 *  Prints a message into the console in a particular color.
 *
 *  @param		str - The text we want to print
 *  @param		color - The color we want to print the text in
 */
void printInColor(std::string str, ConsoleColor color) {
	
	HANDLE  hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, (int) color);
	cout << str;// << endl;
	SetConsoleTextAttribute(hConsole, (int) ConsoleColor::Default);

}

/**
 *  Pauses the code until the user presses enter.
 */
void awaitEnter() {
	cout << "\n\nPress [enter] to continue.";
	string dump;
	getline(cin, dump);
}

/**
 *  Starts up the program and loads in data from files.
 *
 *	@see			loadGlobals()
 *	@see			loadUsers()
 *	@see			loadProjects()
 */
void startUp() {
	if(!is_directory(DATA_ROOT)) 
		create_directories(DATA_ROOT);

	loadGlobals();
	loadUsers();
	loadProjects();
}

/**
 *  Shuts down the program
 */
void shutDown() {

}



/**
 *	Loads all the users into the program.
 *
 *	@see			User::readFromFile()
 */
void loadUsers() {
	path p = USERS_ROOT;

	if(is_directory(p)) {
	
		// Iterate over user folder
		for(const directory_entry& x : directory_iterator{ p }) {
		
			// Get Id
			int id = stoi(x.path().filename());

			// Create user object
			auto user = new User(id);

			// Read data
			user->readFromFile();

			// Add to map
			gUsers[id] = user;
		}

	} else {
		// If the directory doesn't exist: create it
		create_directories(p);
	}
	
}

/**
 *  Loads all the projects into the program.
 *
 *	@see			Project::readFromFile()
 */
void loadProjects() {
	path p = PROJECTS_ROOT;

	if(is_directory(p)) {

		// Iterate over project folder
		for(const directory_entry& x : directory_iterator{ p }) {

			// Get Id
			int id = stoi(x.path().filename());

			// Create user object
			auto project = new Project(id);

			// Read data
			project->readFromFile();

			// Add to map
			gProjects[id] = project;
		}

	} else {
		// If the directory doesn't exist: create it
		create_directories(p);
	}
}


/**
 *  Loads global variables from file
 */
void loadGlobals() {
	// Open file
	ifstream inn;
	inn.open(DATA_ROOT + "info.dta");

	if(!inn) {
		return;
	}

	// Read name
	inn >> gNextId;

	// Close the file
	inn.close();
}

/**
 *  Saves global variables to file
 */
void saveGlobals() {

	path p = DATA_ROOT + "info.dta";

	//if(!exists(p))
		

	ofstream out;
	out.open(DATA_ROOT + "info.dta", ofstream::trunc);

	if(!out) {
		return;
	}

	// Write name
	out << gNextId << endl;

	// Close file
	out.close();
}


/**
 *  Gets the next id to be used and updates the number
 *
 *	@see			saveGlobals()
 */
int getNextId() {
	int res = gNextId++;
	saveGlobals();
	return res;
}



/*
	============================================
		Main menu

		Managing project and stuff
	============================================
 */



 /**
	* Takes in commands from the user.
	*
	*	@see			printMainMenu()
	*	@see			printProjects()
	*	@see			printProjectDesc()
	*	@see			addProject()
	*	@see			removeProject()
	*	@see			editProject()
	*	@see			userMenu()
	*	@see			awaitEnter()
	*/
void mainMenu() {

	printMainMenu();
	char command = lesChar("\nCommand");
	while(command != 'Q') {
		
		// Indicates whether or not to pause after finishing the command.
		bool pauseOnCompletion = true;

		switch(command) {
		case 'P': printProjects();  break;
		case 'D': printProjectDesc();	break;	//Remove this if description in print project
		case 'A': addProject();     break;
		case 'R': removeProject();  break;
		case 'E': editProject();    pauseOnCompletion = false;
																break;
		case 'U': userMenu();				pauseOnCompletion = false;
																break;
		case 'H': helpMenu();				pauseOnCompletion = false;
													      break;
		default: 
			pauseOnCompletion = false; 
			break;
		}

		if(pauseOnCompletion)
			awaitEnter();
		printMainMenu();
		command = lesChar("\nCommand");
	}
}


/**
 *  Prints the options for the main menu out on the screen
 */
void printMainMenu() {
	cout
		//<< "\n--------------------------------"
		<< "\nAvailable commands:"
		<< "\n\tP - Print projects"
		<< "\n\tD - Print descriptions (projects)"	//Remove this if description in print project
		<< "\n\tA - Add project"
		<< "\n\tR - Remove project"
		<< "\n\tE - Edit project"
		<< "\n\tU - Users"
		<< "\n\tH - Help"
		<< "\n\tQ - Quit\n";
}


/**
 *  Prints a list of the projects on the screen
 *
 *	It sorts the projects by status and id
 *
 *	@see			divLine(...)
 *	@see			Project::writeLine()
 */
void printProjects() {
	// Check that there are projects
	if(gProjects.size() == 0) {
		cout << "\nNo projects found!\n";
		return;
	}

	else
	// Display each on the screen
	divLine(menuLen);
	cout << "\n| ID | Name        | Category | Prio | Start       | Deadline    | Finished    |";
	divLine(menuLen);
	

	vector<Project*> projects;
	for(auto pair : gProjects) 
		projects.push_back(pair.second);
	
	// Sort by status, then id or something
	sort(projects.begin(), projects.end(), [](const auto& a, const auto& b) {
		if(a->status < b->status) return true;
		return (a->status == b->status) && (a->id < b->id);
	});

	for(auto proj : projects) 
		proj->writeLine();
	

	divLine(menuLen);
}



/**
 *  Creates a new prject and adds it
 *
 *	@see			getNextId()
 *	@see			Project::readD()
 *	@see			Project::writeToFile()
 */
void addProject() {
	
	int id = getNextId();

	// Create object
	auto* project = new Project(id);

	// Read data
	project->readD();

	// Add object to dict
	gProjects[id] = project;

	project->writeToFile();

	cout << "\tProject [" << project->id << "] " << project->name << " has been created and added.\n";

}



/**
 *  Deletes a project from the program
 *
 *	@see			Project::deleteObject()
 */
void removeProject() {
	// Check size
	if(gProjects.size() == 0) {
		cout << "\nNo projects found!\n";
		return;
	}

	// Get id
	int id = lesInt("\tProject id", -1, gNextId);

	// Cancel if -1
	if(id == -1)
		return;

	// Check that the id is in use
	if(gProjects.find(id) == gProjects.end()) {
		cout << "\tId not in use" << endl;
		return;
	}

	// Remove object
	auto* project = gProjects[id];
	gProjects.erase(id);

	cout << "\tProject [" << project->id << "] " << project->name << " has been removed and deleted.\n";

	project->deleteObject();
	delete project;
}



/**
 *  Gets a project and opens the project menu for it.
 *
 *	@see			projectMenu(...)
 */
void editProject() {
	// Check size
	if(gProjects.size() == 0) {
		cout << "\nNo projects found!\n";
		return;
	}

	// Get id
	int id = lesInt("\tProject id", -1, gNextId);

	// Cancel if -1
	if(id == -1)
		return;

	// Check that the id is in use
	if(gProjects.find(id) == gProjects.end()) {
		cout << "\tId not in use" << endl;
		return;
	}

	// Project menu
	auto* project = gProjects[id];
	projectMenu(project);

}



/**
 *  Lists the projects and their descriptions
 *
 *	@see			divLine(...)
 *	@see			Project::writeDesc()
 */
void printProjectDesc() {
	// Check that there are projects
	if (gProjects.size() == 0) {
		cout << "\nNo projects found!\n";
		return;
	}

	// Display names and descriptions on screen
	divLine(menuLen);
	cout << left << "\n| name         | " << setw(61) << "Description" << " |";
	divLine(menuLen);
	for (auto pair : gProjects) {
		pair.second->writeDesc();
	}
	divLine(menuLen);
}




/*
	============================================
		Project menu

		Managing project and stuff
	============================================
 */


 /**
	* Takes in commands from the user.
	*
	*	@see			editProjectDetail()
	*	@see			printTasks()
	*	@see			addTask()
	*	@see			removeTask()
	*	@see			editTask()
	*	@see			printMembers()
	*	@see			addMember()
	*	@see			awaitEnter()
	*	@see			removeMember()
	*	@see			printProjectMenu()
	*/
void projectMenu(Project* const proj) {

	printProjectMenu(proj);
	char command = lesChar("Command");
	while(command != 'Q') {

		// Indicates whether or not to pause after finishing the command.
		bool pauseOnCompletion = true;

		switch(command) {
		case 'E': editProjectDetail(proj); pauseOnCompletion = false;
																				break;
		case 'P': printTasks(proj);         break;
		case 'A': addTask(proj);            break;
		case 'R': removeTask(proj);         break;
		case 'C': editTask(proj); pauseOnCompletion = false;
																				break;
		case 'M': printMembers(proj);       break;
		case 'N': addMember(proj);          break;
		case 'O': removeMember(proj);       break;
		default: pauseOnCompletion = false; break;
		}

		if(pauseOnCompletion)
			awaitEnter();
		printProjectMenu(proj);
		command = lesChar("Command");
	}

	proj->writeToFile();

}



/**
 *  Prints the options for the project menu
 */
void printProjectMenu(Project* const proj) {
	cout
		<< "\nAvailable commands for '" << proj->id << "':"
		<< "\n\tE - Edit project details"
		<< "\n\tP - Print tasks"
		<< "\n\tA - Add task"
		<< "\n\tR - Remove task"
		<< "\n\tC - Edit task"
		<< "\n\tM - Print member"
		<< "\n\tN - Add member"
		<< "\n\tO - Remove member"
		<< "\n\tQ - Quit\n";
}



/**
 *  Lets the user change the details of the project
 *
 *	@see			awaitEnter()
 *	@see			printProjectDetails(...)
 */
void editProjectDetail(Project* const proj) {

	string print;
	stringstream ss;
	string newName;

	printProjectDetails(proj);
	char command = lesChar("\nCommand");
	while(command != 'Q') {
		switch(command) {
		case 'P':  
			ss << "\tInput new priority (Currently" << proj->priority << ")";
			print = ss.str(); ss.clear();

			proj->priority = lesInt(print.c_str(), 1, 9);
			break;

		case 'N':  
			cout << "\tInput new name (Currently: " << proj->name << "): ";

			// Read new name
			getline(cin, newName);

			// Change name if user gave a new one
			if(newName.length() > 0)
				proj->name = newName;
			break;

		case 'D':  
			cout << "\tInput new description: ";

			// Read new name
			getline(cin, newName);

			// Change name if user gave a new one
			if(newName.length() > 0)
				proj->desc = newName;
			break;

		case 'C':  
			cout << "\tInput new category (Currently: " << proj->category << "): ";

			// Read new name
			getline(cin, newName);

			// Change name if user gave a new one
			if(newName.length() > 0)
				proj->category = newName;
			break;

		case 'S':  
			proj->status = readStatus();
			break;
		case 'B':
			proj->start = readDate("Project starting date");
			break;
		case 'E':
			proj->ended = readDate("Project completion date");
			break;
		case 'F':
			proj->deadline = readDate("Project deadline");
			break;
		}

		awaitEnter();
		printProjectDetails(proj);
		command = lesChar("\nCommand");
	}
}


/**
 *  Prints the choices for editing the project details
 */
void printProjectDetails(Project* const proj) {
	cout
		<< "\nAvailable details for [" << proj->id << "] " << proj->name << ":"
		<< "\n\tP - priority"
		<< "\n\tN - name"
		<< "\n\tD - description"
		<< "\n\tC - category"
		<< "\n\tS - status"
		<< "\n\tB - start"
		<< "\n\tE - ended"
		<< "\n\tF - deadline"
		<< "\n\tQ - Quit\n";
}




/**
 *  Prints a list of all the tasks in the project
 *
 *	@see			divLine()
 *	@see			Task::writeLine()
 */
void printTasks(Project* const proj) {
	// Check that there are tasks
	if(proj->tasks.size() == 0) {
		cout << "\nNo tasks found!\n";
		return;
	}


	vector<Task*> tasks;
	for(auto task : proj->tasks)
		tasks.push_back(task);

	// Sort by status, then id or something
	sort(tasks.begin(), tasks.end(), [](const auto& a, const auto& b) {
		if(a->status < b->status) return true;
		return (a->status == b->status) && (a->order < b->order);
	});


	divLine(menuLen);
	cout << "\n| Nr | Name        | Category | Prio | Start       | Deadline    | Finished    |";
	divLine(menuLen);

	for(auto task : tasks)
		task->writeLine();
	divLine(menuLen);

}


/**
 *  Adds a task to the project
 *
 *	@see			Task::readD()
 *	@see			Project::updateTaskOrder()
 */
void addTask(Project* const proj) {
	// Create object
	Task* task = new Task(getNextId());

	// Read data
	task->readD();

	// Add object to dict
	proj->tasks.push_back(task);

	proj->updateTaskOrder();

	cout << "\tTask [" << task->id << "] " << task->name 
		<< " has been created and added to [" << proj->id << "] " << proj->name << ".\n";

}


/**
 *  Removes a task from the project
 *
 *	@see			Project::updateTaskOrder()
 */
void removeTask(Project* const proj) {
	// Check size
	if(proj->tasks.size() == 0) {
		cout << "\nNo tasks found!\n";
		return;
	}

	// Get id
	int index = lesInt("\tTask nr", 0, proj->tasks.size()) - 1;

	// Cancel if -1
	if(index == -1)
		return;

	// Remove object
	auto* task = proj->tasks[index];


	proj->tasks.erase(proj->tasks.begin() + index);
	proj->updateTaskOrder();

	cout << "\tTask [" << task->id << "] " << task->name
		<< " has been deleted and removed from [" << proj->id << "] " << proj->name << ".\n";

	delete task;
	
}


/**
 *  Edits a task in the project
 *
 *	@see			printTaskDetails(...)
 *	@see			awaitEnter()
 *	@see			Project::updateTaskOrder()
 */
void editTask(Project* const proj) {
	// Check size
	if(proj->tasks.size() == 0) {
		cout << "\nNo tasks found!";
		return;
	}

	// Get id
	int index = lesInt("\tTask nr", 0, proj->tasks.size()) - 1;

	// Cancel if -1
	if(index == -1)
		return;

	// Get task
	auto task = proj->tasks[index];


	string print;
	stringstream ss;
	string newName;
	int newOrder;

	printTaskDetails(task);
	char command = lesChar("\nCommand");
	while(command != 'Q') {
		switch(command) {
		case 'P':
			ss << "\tInput new priority (Currently" << task->priority << ")";
			print = ss.str(); ss.clear();

			task->priority = lesInt(print.c_str(), 1, 9);
			break;

		case 'N':
			cout << "\tInput new name (Currently: " << task->name << "): ";

			// Read new name
			getline(cin, newName);

			// Change name if user gave a new one
			if(newName.length() > 0)
				task->name = newName;
			break;

		case 'D':
			cout << "\tInput new description: ";

			// Read new name
			getline(cin, newName);

			// Change name if user gave a new one
			if(newName.length() > 0)
				task->desc = newName;
			break;

		case 'C':
			cout << "\tInput new category (Currently: " << task->category << "): ";

			// Read new name
			getline(cin, newName);

			// Change name if user gave a new one
			if(newName.length() > 0)
				task->category = newName;
			break;

		case 'S':
			task->status = readStatus();
			break;
		case 'B':  
			task->start = readDate("Task starting date");
			break;
		case 'E':  
			task->ended = readDate("Task completion date");
			break;
		case 'F':  
			task->deadline = readDate("Task deadline");
			break;
		case 'O':  
			ss << "\tInput new order (Currently" << task->order << ")";
			print = ss.str(); ss.clear();

			newOrder = lesInt(print.c_str(), 0, proj->tasks.size()-1);

			// If we want to move the task down, swap it down
			// If we want to move the task up, swap it up
			if(task->order > newOrder) {
				for(int i = task->order; i > newOrder; --i) {
					auto temp = proj->tasks[i];
					proj->tasks[i] = proj->tasks[i - 1];
					proj->tasks[i-1] = temp;
				}
			} else if(task->order < newOrder) {
				for(int i = task->order; i < newOrder; ++i) {
					auto temp = proj->tasks[i];
					proj->tasks[i] = proj->tasks[i + 1];
					proj->tasks[i + 1] = temp;
				}
			}

			// Update the task orders
			proj->updateTaskOrder();
			break;

		default:	 break;
		}

		awaitEnter();
		printTaskDetails(task);
		command = lesChar("\nCommand");
	}
}


/**
 *  Prints the options for what you can change about a project
 */
void printTaskDetails(Task* const task) {
	cout
		<< "\nAvailable details for '" << task->id << "':"
		<< "\n\tP - priority"
		<< "\n\tN - name"
		<< "\n\tD - description"
		<< "\n\tC - category"
		<< "\n\tS - status"
		<< "\n\tB - start"
		<< "\n\tE - ended"
		<< "\n\tF - deadline"
		<< "\n\tO - order"
		<< "\n\tQ - Quit\n";
}





/**
 *  Prints the members of a project
 *
 *	@see			User::writeLine()
 */
void printMembers(Project* const proj) {
	// Check that there are tasks
	if(proj->members.size() == 0) {
		cout << "\nNo members found!\n";
		return;
	}

	// Display each on the screen
	for(auto pair : proj->members) {
		pair.second->writeLine();
	}
	cout << endl;
}


/**
 *  Adds a member to the project
 */
void addMember(Project* const proj) {

	// Get id
	int id = lesInt("\tUser id", -1, gNextId);

	// Cancel if -1
	if(id == -1)
		return;

	// Check that the id is not in use
	if(proj->members.find(id) != proj->members.end()) {
		cout << "\tId already in project" << endl;
		return;
	}

	// Check that the id is a valid user
	if(gUsers.find(id) == gUsers.end()) {
		cout << "\tId already not a valid user" << endl;
		return;
	}

	// Create object
	auto user = gUsers[id];


	// Add object to dict
	proj->members[id] = user;


	cout << "\tUser [" << user->id << "] " << user->name
		<< " has been added to [" << proj->id << "] " << proj->name << ".\n";
}


/**
 *  Removes a member from the project
 */
void removeMember(Project* const proj) {

	// Check size
	if(proj->members.size() == 0) {
		cout << "\nNo projects found!\n";
		return;
	}

	// Get id
	int id = lesInt("\tProject id", -1, gNextId);

	// Cancel if -1
	if(id == -1)
		return;

	// Check that the id is in use
	if(proj->members.find(id) == proj->members.end()) {
		cout << "\tId not in use" << endl;
		return;
	}

	// Remove object
	auto* user = proj->members[id];
	proj->members.erase(id);

	cout << "\tUser [" << user->id << "] " << user->name
		<< " has been removed from [" << proj->id << "] " << proj->name << ".\n";
	// "Delete files" destructor No, use method, destructor deletes at program exit.
}








/*
	============================================
		User menu

		Managing users and stuff
	============================================
 */



 /**
	* Takes in commands from the user.
	*
	*	@see			printUserMenu()
	*	@see			printUsers()
	*	@see			addUser()
	*	@see			removeUser()
	*	@see			editUser()
	*	@see			awaitEnter()
	*/
void userMenu() {

	printUserMenu();
	char command = lesChar("\nCommand");
	while(command != 'Q') {

		// Indicates whether or not to pause after finishing the command.
		bool pauseOnCompletion = true;

		switch(command) {
		case 'P': printUsers();  break;
		case 'A': addUser();     break;
		case 'R': removeUser();  break;
		case 'E': editUser();    pauseOnCompletion = false;
														 break;
		default: 
			pauseOnCompletion = false; 
			break;
		}

		if(pauseOnCompletion)
			awaitEnter();
		printUserMenu();
		command = lesChar("\nCommand");
	}
}


/**
 *  Prints out the options in the user menu
 */
void printUserMenu() {
	cout
		<< "\nAvailable commands:"
		<< "\n\tP - Print users"
		<< "\n\tA - Add user"
		<< "\n\tR - Remove user"
		<< "\n\tE - Edit user"
		<< "\n\tQ - Quit\n";
}


/**
 *  Lists all the users available in the program
 *
 *	@see			User::writeLine()
 */
void printUsers() {
	// Check that there are users
	if(gUsers.size() == 0) {
		cout << "\nNo user found!\n";
		return;
	}

	// Display each on the screen
	for(auto pair : gUsers) {
		pair.second->writeLine();
	}

}

/**
 *  Adds a new user to the program
 *
 *	@see			User::readD()
 *	@see			User::writeToFile()
 *	@see			getNextId()
 */
void addUser() {
	int id = getNextId();

	// Check that the id is not in use
	if(gUsers.find(id) != gUsers.end()) {
		cout << "\tId already in use" << endl;
		return;
	}

	// Create object
	auto* user = new User(id);

	// Read data
	user->readD();

	// Add object to dict
	gUsers[id] = user;

	user->writeToFile();

	cout << "\tUser [" << user->id << "] " << user->name
		<< " has been created.\n";
}

/**
 *  Removes a user from the program
 *
 *	@see			User::deleteObject()
 */
void removeUser() {
	// Check size
	if(gUsers.size() == 0) {
		cout << "\nNo user found!\n";
		return;
	}

	// Get id
	int id = lesInt("\tUser id", -1, gNextId);

	// Cancel if -1
	if(id == -1)
		return;

	// Check that the id is in use
	if(gUsers.find(id) == gUsers.end()) {
		cout << "\tId not in use" << endl;
		return;
	}

	// Remove object
	auto* user = gUsers[id];
	gUsers.erase(id);


	cout << "\tUser [" << user->id << "] " << user->name
		<< " has been deleted.\n";
	// "Delete files" destructor No, use method, destructor deletes at program exit.
	user->deleteObject();
	delete user;

}

/**
 *  Lets the user edit details about a user
 *
 *	@see			printUserDetails(...)
 *	@see			awaitEnter()
 */
void editUser() {
	// Check size
	if(gUsers.size() == 0) {
		cout << "\nNo user found!\n";
		return;
	}

	// Get id
	int id = lesInt("\tUser id", -1, gNextId);

	// Cancel if -1
	if(id == -1)
		return;

	// Check that the id is in use
	if(gUsers.find(id) == gUsers.end()) {
		cout << "\tId not in use" << endl;
		return;
	}

	auto* user = gUsers[id];
	

	// Variables used in the switch
	string newName = "";


	printUserDetails(user);
	char command = lesChar("Command");
	while(command != 'Q') {

		switch(command) {
		case 'N': 
			cout << "\tUser's new name (Currently: " << user->name << "): "; 
			
			// Read new name
			getline(cin, newName);

			// Change name if user gave a new one
			if(newName.length() > 0)
				user->name = newName;

			break;
		}

		awaitEnter();
		printUserDetails(user);
		command = lesChar("Command");
	}
}


/**
 *  Prints the details about a user that can be changed
 */
void printUserDetails(User* const user) {
	cout
		<< "\nAvailable details for [" << user->id << "] " << user->name << ":"
		<< "\n\tN - name"
		<< "\n\tQ - Quit\n";
}




/*
	============================================
		Help menu

		Holds help articles on parts of the program.
	============================================
 */





 /**
	* Takes in commands from the user.
	*
	*	@see			printHelpMenu()
	*	@see			helpUser()
	*	@see			helpProject()
	*	@see			helpTask()
	*	@see			awaitEnter()
	*/
void helpMenu() {

	printHelpMenu();
	char command = lesChar("\nCommand");
	while(command != 'Q') {

		// Indicates whether or not to pause after finishing the command.
		bool pauseOnCompletion = true;

		switch(command) {
		case 'U': helpUser();			break;
		case 'P': helpProject();	break;
		case 'T': helpTask();			break;
		default:  
			pauseOnCompletion = false;
			break;
		}

		if(pauseOnCompletion)
			awaitEnter();
		printHelpMenu();
		command = lesChar("\nCommand");
	}
}

/**
 *  Prints options for the help menu
 */
void printHelpMenu() {
	cout
		<< "\nAvailable commands:"
		<< "\n\tU - Users"
		<< "\n\tP - Projects"
		<< "\n\tT - Tasks"
		<< "\n\tQ - Quit\n";
}

/**
 *  Prints help page for the users on the program
 */
void helpUser() {
	cout
		<< "\nUser help:"
		<< "\n\tUsers represent the people working on the projects."
		<< "\n\tYou can assign them to be members of a project when editing that project."
		<< endl;
}

/**
 *  Prints help page for the projects on the program
 */
void helpProject() {
	cout
		<< "\nProject help:"
		<< "\n\tProjects are made up of a series of tasks that need to be done and members that will do them."
		<< "\n\tYou can also set priority, status and deadline for a project."
		<< "\n\tWhen printed, the projects will be colored and sorted according to their status."
		<< endl;
}

/**
 *  Prints help page for the tasks on the program
 */
void helpTask() {
	cout
		<< "\nTask help:"
		<< "\n\tTasks are the spesific jobs that need to be done in order to progress a project."
		<< "\n\tTasks can be added/removed/changed/reordered in the project menu of the project they belong to."
		<< "\n\tWhen listed, tasks will be ordered and colored by their status."
		<< endl;
}

