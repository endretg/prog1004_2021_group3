#pragma once

#include <vector>
#include <ctime>
#include <string>
#include <iostream>
#include <fstream> 
#include "Enum.h"
#include "Class.h"
#include "Struct.h"


class Task {
public:
	int id,
		priority,
		order;
	std::string name,
		desc,
		category;
	Date start,
		ended,
		deadline;
	Status      status = Status::ToDo;


	Task() {}
	Task(int id) { this->id = id; }

	void readD();
	void writeD();
	void writeLine();

	void writeToFile(std::ofstream& out);
	void readFromFile(std::ifstream& inn);

};