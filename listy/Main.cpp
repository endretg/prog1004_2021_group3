#include <iostream>
#include <iomanip>
#include <ctime>
#include <string>
#include <fstream>

#include "Enum.h"
#include "Funct.h"
#include "LesData3.h"

#include <windows.h>   // WinApi header


using namespace std;


int gNextId = 0;

std::map<int, Project*> gProjects;	///< List of all projects in the program
std::map<int, User*> gUsers;				///< List of all users in the program



int main() {
	startUp();
	mainMenu();
	shutDown();
	return 0;
}
