#pragma once



enum class Status {
	Failed,
	ToDo,
	Started,
	Completed,
};

enum class ConsoleColor {
	White		= 7,
	Blue		= 9,
	Green		= 10,
	Teal		= 11,
	Red			= 12,
	Pink		= 13,
	Yellow	= 13,

	Default	= White,
};
