
#include <iostream>
#include <fstream> 
#include <sstream> 
#include <string> 
#include <filesystem>
#include "User.h"
#include "Const.h"


using namespace std;
using namespace std::filesystem;


/**
 *	Reads info about the user from the user.
 */
void User::readD() {
	cout << "\tUser's name: "; getline(cin, name);

}

/**
 *	Writes info about this user on the screen.
 */
void User::writeD() {}


/**
 *	Writes a line of info about this user on the screen.
 */
void User::writeLine() {
	std::cout << "\n\t" << id << ": " << name;
}


/**
 *	Deletes the file belonging to this user
 *
 *  @see  User::getPath()
 */
void User::deleteObject() {
	remove(getPath());
}

/**
 *	Writes info about this user to file
 *
 *  @see  User::getPath()
 */
void User::writeToFile() {
	ofstream out;
	out.open(getPath(), ofstream::trunc);

	if(!out) {
		return;
	}

	// Write name
	out << name << endl;
	
	// Close file
	out.close();
}

/**
 *	Reads info about this user from file
 *
 *  @see  User::getPath()
 */
void User::readFromFile() {

	// Open file
	ifstream inn;
	inn.open(getPath());

	if(!inn) {
		return;
	}

	// Read name
	getline(inn, name);

	// Close the file
	inn.close();

}

/**
 *	Gets the path of this user's file
 *
 *	@return		The path to this user's file
 */
string User::getPath() {
	ostringstream oss;
	oss << USERS_ROOT << id << ".dta";
	return oss.str();
}
