#pragma once

#include <vector>
#include <ctime>
#include <string>
#include <map>
#include <fstream> 
#include "Enum.h"
#include "Class.h"
#include "Struct.h"



class Project {
public:
	int id,
		priority;
	std::string name,
		desc,
		category;
	Status status = Status::ToDo;
	Date start,
		ended,
		deadline;
	std::vector<Task*> tasks;
	std::map<int, User*> members;


	Project() {}
	Project(int id) { this->id = id; }

	void readD();
	void writeD();
	void writeLine();
	void writeDesc();

	void deleteObject();
	void writeToFile();
	void readFromFile();

	std::string getPath();

	void updateTaskOrder();


private:
	void writeTasks(std::ofstream& out);
	void readTasks(std::ifstream& inn);

	void writeDetails(std::ofstream& out);
	void readDetails(std::ifstream& inn);
};