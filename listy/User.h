#pragma once

#include <vector>
#include <ctime>
#include <string>
#include "Enum.h"
#include "Class.h"


class User {
public:
	int id;
	std::string name;


	User() {}
	User(int id) { this->id = id; }

	void readD();
	void writeD();
	void writeLine();

	void deleteObject();
	void writeToFile();
	void readFromFile();

	std::string getPath();
};