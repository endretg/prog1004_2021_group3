# LISTY USER MANUAL:

### This brief guide will go through all the options you have while using “Listy”.

Glossary:
Tasks: Is a collection of data like ID, name and description. Tasks can be stored in a chosen project
Project: A project in Listy is a group of tasks. Additionally the Project itself has the same elements as a task.
User: Different users can be assigned to different projects.
-1: in any of the command where you are prompted with “(-1 – <some number>)” -1 means abort.

- Print projects: Writes all the data of all the tasks except the descriptions.
- Print descriptions: Writes the descriptions of all projects.
- Add project/task: Creates a new project/task.
	Add task and add project are similar enough to both be covered here:
	First step is to decide priority from 1-9. Next you type in the name you want the project/task 	to have. When prompted with “Task description” you type in a shirt description of your 	project/task. Category can be any keyword you associate with the task. The date should be 	structured in this way “dd/mm/yyyy”. You do not need to type the quotation marks when 	entering the date.
- Remove project: Removes a project by entering that project’s unique ID.

- Edit project: Here you will be sent to another menu with different options after choosing project:
	- Edit project details: Sends you through the same process as when creating a new project. 	  Overwriting the data already there with the new user-input.
	- Print tasks: Prints most of the info for the tasks in the selected project.
	- Add task: @see add project/task.
	- Remove task: Removes the task with the selected ID.
	- Edit task: Same as @see Edit project details, but for tasks.
	- Print member: writes all the users responsible for the project.
	- Add member: Adds a user to the project. This user will then be responsible for the selected 	  Project. Multiple users can be added to a Project.
	- Remove member: Removes a user from the selected task.

- Users: This command displays the user-menu, where you can do the following:
	- Print users: Writes all the data for all the users.
	- Add user: Adds a new user with a chosen name and a unique ID.
	- Remove user: deletes a user with a specific ID
	- Edit user: This is the same process ass making a new user, but edits the data of a previously 	  added user.

- Quit: Saves the changes you have made and exits the program.
